#include <PineDio/LoRa/Communicator.h>
#include <iostream>
#include <future>
#include <algorithm>
#include <cctype>
using namespace PineDio::LoRa;

namespace {
std::string GetString() {
  std::string input;
  std::getline(std::cin, input);
  return input;
}
}

Communicator::Communicator(PineDio::LoRa::PinedioLoraRadio &radio) : radio{radio} {
  radio.Initialize();
  receiveTask.reset(new std::thread([this](){Receive();}));
}

Communicator::~Communicator() {
  running = false;
  receiveTask->join();
}

void Communicator::Run() {
  running = true;

  std::future<std::string> futureString = std::async(std::launch::async, GetString);


  while(running) {
    if(futureString.wait_for(std::chrono::milliseconds {0}) == std::future_status::ready) {
      auto msgStr = futureString.get();
      std::vector<uint8_t> msg;

      std::vector<uint8_t> msgChunk;
      auto msgSize = msgStr.size();
      int chunkStart = 0;
      int msgCount = 0;
      unsigned char msgHeader[] = "EPP";
      unsigned char msgEOT[]= "EOT";
      unsigned char msgPadding = ' ';
      if(msgSize < 19) {
        msgCount = 1;
      } else {
        msgCount = (msgSize + 19) / 18;
      }
      for(int i=0;i<msgCount;i++) {
        for(int j=chunkStart;j<chunkStart+18;j++) {
          if(isalnum(msgStr[j]) || std::ispunct(msgStr[j])) {
            msgChunk.push_back(msgStr[j]);
          } else {
            msgChunk.push_back(msgPadding);
          }          
        }
        chunkStart = chunkStart + 18;
        for(auto h : msgHeader)
          msg.push_back(h);
        for(auto c : msgChunk)
          msg.push_back(c);
        if(i==msgCount - 1) {
          for(auto t : msgEOT)
            msg.push_back(t);
        }
        msg.push_back('\0');
        radio.Send(msg);
        msgChunk.clear();
        msg.clear();
        std::this_thread::sleep_for(std::chrono::milliseconds{5000});
      }

      futureString = std::async(std::launch::async, GetString);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds{1000});
  }

}
void Communicator::Receive() {
  std::vector<uint8_t> msgChunk;
  while(running){
    auto data = radio.Receive(std::chrono::milliseconds{100});
    if(data.empty())
      continue;
    if(data[0] == 'E' && data[1] == 'P' && data[2] == 'P') {
      for(int i=3;i<22;i++) {
        msgChunk.push_back(data[i]);
      }
      if(data[22] == 'E' && data[23] == 'O' && data[24] == 'T') {
        for(auto d : msgChunk) {
          std::cout << d;
        }
        std::cout << std::endl;
        msgChunk.clear();
      }
    }
  }
}

void Communicator::Stop() {
  running = false;
}

